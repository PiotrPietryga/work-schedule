import { createStore } from "vuex";

export default createStore({
  state: {
    employees: [],
    myEmployees: [],
    calendar: [],
    weekCounter: 0
  },
  mutations: {
    weekIncrement(state) {
      state.weekCounter++;
    },
    weekDecrement(state) {
      state.weekCounter--;
    }
  },
  actions: {},
  modules: {},
  getters: {
    getEmployees: state => state.employees,
    getMyEmployees: state => state.myEmployees,
    getCalendar: state => state.calendar
  }
});
