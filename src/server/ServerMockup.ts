import faker from "faker";
import _ from "lodash";
import * as config from "@/assets/config.json";

export default class ServerMockup {
  getDob() {
    const date = faker.date.between("1960-01-01", "2003-01-01");
    const day = date.getDay() <= 10 ? `0${date.getDay() + 1}` : date.getDay();
    const month = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
    return `${day}-${month}-${date.getFullYear()}`;
  }

  generateData() {
    const numberOfRepetitions: number = config.numberOfGeneratedEmployees;
    if (
      config.hardcodedData &&
      Array.isArray(config.hardcodedData.employees) &&
      config.hardcodedData.employees.length > 0
    ) {
      return config.hardcodedData;
    }

    return {
      employees: _.times(numberOfRepetitions, (n: number) => {
        return {
          id: n + 1,
          name: faker.name.firstName(),
          surname: faker.name.lastName(),
          dob: this.getDob(),
          avatar: faker.image.image(),
          salary: faker.finance.amount(10, 30),
          status: "unemployed"
        };
      })
    };
  }
}
