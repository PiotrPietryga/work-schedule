import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Employees from "../views/Employees.vue";
import Calendar from "../views/Calendar.vue";
import NotFound from "../views/NotFound.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: "employees",
    component: Employees
  },
  {
    path: "/employees",
    name: "Employees",
    component: Employees
  },
  {
    path: "/calendar",
    name: "Calendar",
    component: Calendar
  },
  {
    path: "/:notFound(.*)",
    name: "NotFound",
    component: NotFound
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
