import { createApp } from "vue";
import { Server } from "miragejs";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import ServerMockup from "./server/ServerMockup";

new Server({
  seeds(server) {
    server.db.loadData({
      employees: new ServerMockup().generateData().employees,
      myEmployees: [],
      calendar: [
        {
          id: 1,
          calendar: {
            "21-02-2021": ["04:00 - 08:00", "08:00 - 12:00"],
            "22-02-2021": ["04:00 - 08:00"],
            "23-02-2021": ["04:00 - 08:00"],
            "24-02-2021": ["04:00 - 08:00"]
          }
        },
        {
          id: 2,
          calendar: {
            "21-02-2021": ["12:00 - 16:00", "16:00 - 20:00"],
            "22-02-2021": ["08:00 - 12:00"],
            "23-02-2021": ["04:00 - 08:00"],
            "24-02-2021": ["04:00 - 08:00"]
          }
        },
        {
          id: 3,
          calendar: {
            "23-02-2021": ["04:00 - 08:00"],
            "24-02-2021": ["04:00 - 08:00"]
          }
        }
      ]
    });
  },

  routes() {
    this.namespace = "api";

    this.get("/employees", schema => schema.db.employees);

    this.post("/employees", (schema, request) => {
      const employee = JSON.parse(request.requestBody).data;
      return schema.db.employees.insert({
        name: employee.name,
        surname: employee.surname,
        dob: employee.dob,
        avatar: employee.avatar,
        salary: employee.salary,
        status: employee.status
      });
    });

    this.delete("/employees/:id", (schema, request): any =>
      schema.db.employees.remove(request.params.id)
    );

    this.get("/my-employees", schema => schema.db.myEmployees);

    this.post("/my-employees", (schema, request) => {
      const employee = JSON.parse(request.requestBody).data;
      return schema.db.myEmployees.insert({
        id: employee.id,
        name: employee.name,
        surname: employee.surname,
        dob: employee.dob,
        avatar: employee.avatar,
        salary: employee.salary,
        status: employee.status
      });
    });

    this.delete("/my-employees/:id", (schema, request): any =>
      schema.db.myEmployees.remove(request.params.id)
    );
    this.get("/calendar/:id", (schema, request): any => schema.db.calendar.find(request.params.id));

    this.get("/calendar", schema => schema.db.calendar);
  }
});

createApp(App)
  .use(store)
  .use(router)
  .mount("#app");
