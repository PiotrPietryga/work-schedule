# Schedule.vue

## Installation

Tested on version [Node.js](https://nodejs.org/) v12.8.0 and npm v6.10.2. Deploy at this version or any higher.

```sh
git clone https://gitlab.com/PiotrPietryga/work-schedule.git
cd work-schedule
npm install
npm run start
```

## Fake API and config.js

Schedule.vue uses Fake API to imitate server and HTTP request. You can change some of options by changing values in [config.json](https://gitlab.com/PiotrPietryga/work-schedule/-/blob/master/src/assets/config.json) file.

| Parameter                    | Description                                                                                                |
| ---------------------------- | ---------------------------------------------------------------------------------------------------------- |
| `numberOfGeneratedEmployees` | Number of generated people ready to work. Default value is 30.                                             |
| `hardcodedData`              | Object with hardcoded data. Now supports `employees` array which can contain array of employees objects. If array is empty, Fake API generates data automatically |

## Example of object for `employees` in `hardcodedData` in config.json file

```json
{
  "id": 1,
  "name": "Mikołaj",
  "surname": "Kowalski",
  "dob": "01-01-1970",
  "age": 23,
  "avatar": "https://cdn.pixabay.com/photo/2018/08/28/12/41/avatar-3637425_960_720.png",
  "salary": 21,
  "status": "unemployed"
},
{
  "id": 2,
  "name": "Rafał",
  "surname": "Nowak",
  "dob": "09-04-1986",
  "age": 44,
  "avatar": "https://cdn.pixabay.com/photo/2018/08/28/12/41/avatar-3637425_960_720.png",
  "salary": 28,
  "status": "unemployed"
},
{
  "id": 3,
  "name": "Michał",
  "surname": "Nowakowski",
  "dob": "11-11-1991",
  "age": 19,
  "avatar": "https://cdn.pixabay.com/photo/2018/08/28/12/41/avatar-3637425_960_720.png",
  "salary": 17,
  "status": "unemployed"
}
```

## Supported Browsers
- Google Chrome
- Mozilla Firefox
- Opera
- Microsoft Edge
